import React from 'react';
import 'antd/dist/antd.css';
import { Input } from 'antd';


const SearchAccount = ({onChange}) => {
    return (
        <Input 
        onChange={onChange} 
        style={{ width: 300 }} 
        placeholder="account" />
    )};
  
  export default SearchAccount;
