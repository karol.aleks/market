export const END_POINT = 'https://apieos.token.store/orders';

export const markets = {
    'parslseed123-seed-eos': 'SEED',
    'trybenetwork-trybe-eos': 'TRYBE',
    'eosdragontkn-dragon-eos': 'DRAGON',
  }
