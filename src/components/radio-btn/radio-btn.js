import React from 'react';
import { Radio } from 'antd';
import { markets } from '../basic';

const RadioBtn = ({ market, onChange }) => {
  return (
    <div>
      <div>
        <Radio.Group defaultValue={market} buttonStyle="solid">
          {Object.entries(markets).map(([key, value]) => (
            <Radio.Button key={key} value={key} onChange={onChange}>{value}</Radio.Button>
          ))}
        </Radio.Group>
      </div>
    </div>
  )};

  export default RadioBtn;
