import React, {Component} from 'react';
import AppHeader from '../app-header';
import RadioBtn from '../radio-btn';
import SideSelect from '../side-select';
import SearchAccount from '../search-account';
import OrdersTable from '../orders-table';
import {END_POINT} from '../basic';

import './app.css';

class App extends Component {
  state = {
    market: 'parslseed123-seed-eos',
    side: null,
    loading: true,
    data: [],
    term: '',
  }

 async fetchOrders (url) {
  await fetch(url)
  .then(orders => orders.json())
  .then(orders => {
    this.setState({
      loading: false,
      data: [...orders],
    })
  })
 }

  componentDidMount() {
    this.fetchOrders(`${END_POINT}?market=${this.state.market}`)
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.market !== this.state.market ||
      prevState.side !== this.state.side || 
      prevState.term !== this.state.term) {
      this.setState({
        loading: true,
      });
      const marketQuery = `?market=${this.state.market}`;
      const sideQuery = (this.state.side) ? `&side=${this.state.side}` : '';
      const accountQuery = (this.state.term) ? `&account=${this.state.term}` : '';
      const url = `${END_POINT}${marketQuery}${sideQuery}${accountQuery}`
    
      this.fetchOrders(url);
    }
  }

  changeMarket = ({ target: { value }}) => {
    this.setState({
      market: value,
    })
  }

  changeSide = (value) => {
    this.setState({
      side: value,
    })
  }

  changeInput = ({ target: { value }}) => {
    this.setState({
      term: value,
    })
  }
  
  render() {
    const { data, loading, market, term } = this.state;

    return (
      <div className="eos-app">
        <AppHeader/>
        <RadioBtn market={market} onChange={this.changeMarket} />
        <SideSelect onChange={this.changeSide} />
        <SearchAccount onChange={this.changeInput}/>
        <OrdersTable loading={loading} data={data} />
      </div>
    );
  }
}


export default App;
