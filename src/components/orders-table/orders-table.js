import React from 'react';
import { Table } from 'antd';
import 'antd/dist/antd.css';

const fillData = (data = []) => data.map(({
  id,
  account,
  side,
  amount,
  filledAmount,
  price,
  total,
  fee,
  createdAt
}) => ({
  key: id,
  account,
  side,
  price,
  amount,
  total,
  date: createdAt,
  filled: filledAmount,
}));

const columns = [
  {
    title: 'Account ',
    dataIndex: 'account',
    key: 'account',
  },
  {
    title: 'Side ',
    dataIndex: 'side',
    key: 'side',
  },

  {
    title: 'Price',
    dataIndex: 'price',
    key: 'price',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.account - b.account,
  },
  {
    title: 'Amount ',
    dataIndex: 'amount',
    key: 'amount',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.amount - b.amount,
  },
  {
    title: 'Total ',
    dataIndex: 'total',
    key: 'total',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.total - b.total,
  },
  {
    title: 'Date ',
    dataIndex: 'createdAt',
    key: 'createdAt',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.createdAt - b.createdAt ,
  },
];

const OrdersTable = ({ loading, data }) => {
    return <Table
      columns={columns}
      dataSource={data}
      loading={loading}
    />
}

export default OrdersTable;
