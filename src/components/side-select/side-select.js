import React from 'react';
import 'antd/dist/antd.css';
import { Select } from 'antd';

const { Option } = Select;


const SideSelect = ({onChange}) => {
    return (
        <Select
        showSearch
        style={{ width: 300 }}
        placeholder="Select a side"
        optionFilterProp="children"
        onChange={onChange}
        filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
      >
        <Option value="sell">Sell</Option>
        <Option value="buy">Buy</Option>
      </Select>
    )};
  
  export default SideSelect;
